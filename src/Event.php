<?php
namespace vendor\pillax\event\src;

/**
 * Class event
 * Event emitter
 */
class Event {

    /** @var array */
    private static $events = [];

    /**
     * Add event
     *
     * @param string $name
     * @param \Closure $closure
     */
    public static function listen(string $name, \Closure $closure) : void {
        self::$events[$name][] = $closure;
    }

    /**
     * Execute user defined function
     *
     * @param string $name
     */
    public static function dispatch(string $name) : void {
        if(isset(self::$events[$name])) {
            foreach (self::$events[$name] AS $event) {
                call_user_func($event);
            }
        }
    }

}